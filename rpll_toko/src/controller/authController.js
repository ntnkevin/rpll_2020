import firebase from "firebase";
import "firebase/firestore";
import "firebase/auth";

export const createUserAuth = (data) => {
  const db = firebase.firestore();
  db.auth().createUserWithEmailAndPassword(data.email, data.password);
};

export const addDataToDb = (data) => {
  const db = firebase.firestore();
  db.collection("account")
    .add({
      name: data.name,
      email: data.email,
      alamat: data.alamat,
      noTelepon: data.noTelepon,
      photoUrl:
        "https://firebasestorage.googleapis.com/v0/b/rpll-2020.appspot.com/o/Profile%2F363639-200.png?alt=media&token=3f98cecf-7271-4631-98d0-19d1dfe2c014",
    })
    .then(() => {
      console.log("Data added to DB");
    });
};

export const addWishLish = (data) => {
  const db = firebase.firestore();
  db.collection("wishList").add({
    owner: data.email,
    items: [],
  });
};

export const editUserData = (data) => {
  var user = firebase.auth().currentUser;
  user
    .updateProfile({
      displayName: data.name,
      phoneNumber: data.noTelepon,
    })
    .catch(function (error) {
      console.log("Error when edit UserData " + error);
    });
};

export const signIn = async (data) => {
  try {
    const result = await firebase
      .auth()
      .signInWithEmailAndPassword(data.email, data.password);
    // console.log("signed in!");
    return result;
    // return result;
  } catch (error) {
    // alert(error);
    throw error;
  }
};

export const signUp = async (data) => {
  try {
    await firebase.auth().signInWithEmailAndPassword(data.email, data.password);
    // console.log("signed in!");
  } catch (error) {
    console.log("Sign In error : " + error);
  }
};

export const signOut = () => {
  firebase
    .auth()
    .signOut()
    .then(function () {
      alert("LOGOUT");
    })
    .catch(function (error) {
      console.log(error);
    });
};
