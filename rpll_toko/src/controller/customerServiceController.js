import firebase from "firebase";
import "firebase/firestore";

export const addDataToDb = (data) => {
  const db = firebase.firestore();
  db.collection("customerService")
    .add({
      email: data.email,
      custTxt: data.custTxt,
    })
    .then(() => {
      console.log("Data added to DB");
    });
};
