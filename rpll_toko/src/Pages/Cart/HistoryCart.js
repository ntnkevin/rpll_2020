import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import MainContex from "../mainContex";
import firebase from "firebase/app";
import "firebase/firestore";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "50px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {},
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));

const ListOrder = () => {
  const classes = useStyle();
  const history = useHistory();
  const contex = React.useContext(MainContex);
  const [menuList, setMenuList] = React.useState([]);

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("order")
      .where("name", "==", contex.email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            docId: doc.id,
            status: doc.data().status,
          };
          result.push(item);
        });
      });
    setMenuList(result);
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, []);

  const PaidButton = () => {
    return (
      <Button style={{ backgroundColor: "rgba(27,27,27,0.8)", color: "white" }}>
        Already Paid
      </Button>
    );
  };

  const UnpaidButton = (props) => {
    return (
      <Button
        style={{ backgroundColor: "rgba(27,27,27,0.8)", color: "white" }}
        onClick={() => {
          history.push(`/payment-order/${props.docId}`);
        }}
      >
        Pay here
      </Button>
    );
  };


const Order = () => {
  
  return (
    <TableContainer component={Paper}>
    <Table className={classes.table} aria-label="customized table">
      <TableHead>
        <TableRow>
          <StyledTableCell>Order ID</StyledTableCell>
          <StyledTableCell align="right">Status</StyledTableCell>
          <StyledTableCell align="right">Pay Here</StyledTableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {menuList.map((item, key) => (
          <StyledTableRow key={key}>
            <StyledTableCell>{item.docId}</StyledTableCell>
            <StyledTableCell align="right">{item.status}</StyledTableCell>
            <StyledTableCell align="right">{item.status === "PAID" ? (
                  <PaidButton />
                ) : (
                  <UnpaidButton docId={item.docId} />
                )}
            </StyledTableCell>
          </StyledTableRow>
          ))}
      </TableBody>
    </Table>
  </TableContainer>
  );
};

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <h2 style={{
                    fontStyle: "Times New Roman",
                    color:"black",
                    paddingLeft: "15px",
                  }}>Hello, {contex.email}</h2>
        <h2 style={{
                    fontStyle: "Times New Roman",
                    color:"black",
                    paddingLeft: "15px",
                  }}>Here is your list order : </h2>
        <Order />
      </div>
    </div>
    );
}

export default ListOrder;