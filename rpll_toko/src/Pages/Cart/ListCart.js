import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import firebase from "firebase/app";
import "firebase/firestore";
import { confirmAlert } from "react-confirm-alert";
import MainContex from "../mainContex";
import { useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";


const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "50px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {},
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));
const ListCartPage = () => {
  const classes = useStyle();
  const [menuList, setMenuList] = React.useState([]);
  const contex = React.useContext(MainContex);
  const addOrderContex = React.useContext(MainContex);
  const [totalPrice, setTotalPrice] = React.useState([]);

  



  const fetchMenu = async () => {
    const db = firebase.firestore();
    var email = contex.email;
    const result = [];
    const tempTotalPrice = [];

    await db
      .collection("cart")
      .where("owner", "==", contex.email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            id: doc.id,
            itemName: doc.data().item.nameProduct,
            itemQty: doc.data().item.qty,
            owner: doc.data().owner,
            price: doc.data().price,
          };
          tempTotalPrice.push(doc.data().price);
          result.push(item);
        });
      });

      setMenuList(result);
      setTotalPrice(tempTotalPrice);
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, [contex.email]);

  const refreshPage = () => {
    window.location.reload(false);
  };

  const deleteItem = async (idVendor) => {
    const db = firebase.firestore();
    await db.collection("vendor").doc(idVendor).delete();
    refreshPage();
  };

  const deleteHandler = (idVendor) => {
    confirmAlert({
      title: "Confirm to Delete Vendor",
      message: "Are you sure to do this?",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteItem(idVendor),
        },
        {
          label: "No",
          onClick: () => alert("Cancel to Delete"),
        },
      ],
    });
  };

  const history = useHistory();

  const addToOrder = () => {
    const db = firebase.firestore();
    const add = (a, b) => a + b;
    const sum = totalPrice.reduce(add);

    db.collection("order")
      .add({
        
        item: menuList,
        name: contex.email,
        totalBilled: sum,
        
        status: "pending",
      })
      .then((docRef) => {
        deleteCart();
        alert(`DATA ADDED`);
        console.log("Data added to DB");
        history.push(`/payment-order/${docRef.id}`);
      });
  };

  const resetHandler = () => {
    confirmAlert({
      title: "Confirm for reset cart data",
      message: "Are you sure want to clear all product?",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteCart() ,
          
        },
        {
          label: "No",
          onClick: () => alert("Cancel to reset cart data"),
        },
      ],
    });
  };
  
  const deleteCart = () => {
    const db = firebase.firestore();
    
    var cart = db.collection("cart").where("owner", "==", contex.email);
    cart.get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
    doc.ref.delete();

    
  });

});
  };

  const confirmHandler = () => {
    confirmAlert({
      title: "Confirm to make Order?",
      message: "Are you sure to do this? *order can't be edit",
      buttons: [
        {
          label: "Yes",
          onClick: () => addToOrder(),
        },
        {
          label: "No",
          onClick: () => alert("Cancle to Delete"),
        },
      ],
    });
  };

  const Cart = () => {
    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="spanning table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Qty</TableCell>
              <TableCell align="right">Total Price</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {menuList.map((item, key) => (
              <TableRow key={key}>
                <TableCell>{item.itemName}</TableCell>
                <TableCell align="right">{item.itemQty}</TableCell>
                <TableCell align="right">{item.price}</TableCell>
                <TableCell align="right"> </TableCell>
              </TableRow>
            ))}
            <Button
              style={{
                marginTop: "20px",
                marginLeft: "420px",
                marginBottom: "20px",
                backgroundColor: "rgba(27,27,27,0.8)",
                color: "white",
              }}
              onClick={confirmHandler}
              > Confirm Order
              </Button>
              <Button
                    style={{
                      backgroundColor: "rgba(27,27,27,0.8)",
                      color: "white",
                      marginLeft: "40px",
                    }}
                    onClick={() => {
                      resetHandler();
                    }}
                  >
                    Reset Cart
                  </Button>
          </TableBody>
        </Table>
      </TableContainer>
    );  
  };

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
          <h3  style={{
                    fontStyle: "Times New Roman",
                    color:"black",
                    paddingLeft: "15px",
                  }}
          >Account: {contex.email}</h3>
         <h2  style={{
                    fontStyle: "Times New Roman",
                    color:"black",
                    paddingLeft: "15px",
                    textAlign: "center"
                  }}
          >My Cart</h2>
         
          <Cart />
          
        
      </div>
    </div>
  );
};

export default ListCartPage;
