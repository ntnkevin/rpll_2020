import React from "react";
import MainContex from "../mainContex";
import { TextareaAutosize } from "@material-ui/core";
import { addDataToDb } from "../../controller/customerServiceController";

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles }  from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import "firebase/auth";
import { Link } from "react-router-dom";
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        RPLL
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))


const CustomerServicePage = () => {
  const classes = useStyles();
  const contex = React.useContext(MainContex);
  const [txtCustService, setTxtCustService] = React.useState("");

  const dataUser = {
    email: contex.email,
    custTxt: txtCustService,
  };

  const refreshPage = () => {
    window.location.reload(false);
  };

  const submitHandler = () => {
    addDataToDb(dataUser);
    alert("Thx for your support!");
  };

  
  
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Welcome {contex.email}
        </Typography>
        <form className={classes.form} noValidate>
        <TextareaAutosize className ="text-area"
        aria-label="minimum height" 
        rowsMin={10}
        placeholder="Enter your feedback"
        onChange={(ev) => {
          setTxtCustService(ev.target.value);
        }}
      />
          <Button onClick={submitHandler}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Submit Feedback
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default CustomerServicePage;
