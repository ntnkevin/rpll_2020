import React from "react";
import MainContex from "../mainContex";

const AdminUser = () => {
  return (
    <div>
      <h1> Hello ADMIN</h1>
    </div>
  );
};

const NonAdmin = () => {
  return (
    <div>
      <h1> You are not Admin</h1>
      <h1> Go To Home</h1>
    </div>
  );
};

const Admin = () => {
  const contex = React.useContext(MainContex);
  return (
    <div>
      <h1>Admin Page</h1>
      {contex.email == "admin@gmail.com" ? <AdminUser /> : <NonAdmin />}
    </div>
  );
};

export default Admin;
