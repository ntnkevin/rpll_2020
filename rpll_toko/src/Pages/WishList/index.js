import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import firebase from "firebase/app";
import "firebase/firestore";
import MainContex from "../mainContex";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const WishListPage = () => {
  
  const useStyle = makeStyles((theme) => ({
    root: {
      backgroundColor: "#f2f2f2",
      paddingBottom: "15px",
      paddingTop: "20px",
    },
    header: {
      textAlign: "center",
    },
    divbig: {
      margin: "0px auto",
      marginTop: "60px",
      marginBottom: "0px",
      paddingTop: "50px",
      width: "1000px",
      backgroundColor: "#fff",
    },
    content: {},
    row: {
      float: "left",
      width: "50%",
      padding: "10px",
    },
    column: {
      content: "",
      display: "table",
      clear: "both",
    },
    table: {
      width: "50%",
      marginLeft: "300px",
    }
  }));

  const [wishList, setWishList] = React.useState({ items: [] });
  const classes = useStyle();
  const contex = React.useContext(MainContex);
  const history = useHistory();

  const fetchWishList = async () => {
    const db = firebase.firestore();
    var email = contex.email;
    await db
      .collection("wishList")
      .where("owner", "==", email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            id: doc.id,
            items: doc.data().items,
            owner: doc.data().owner,
          };
          setWishList(item);
        });
      });
  };

  React.useEffect(() => {
    fetchWishList();
    //eslint-disable-next-line
  }, [contex.email]);

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell align="right">Item Name</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {wishList.items.map((item, key) => (
            <TableRow key={key}>
              <TableCell component="th" scope="row">
              <img src={item.url} style ={{width:"10%", height:"10%"}}></img>
              </TableCell>
              <TableCell align="right">{item.nameProduct}</TableCell>
              <TableCell align="right">{item.price}</TableCell>
            </TableRow>
          ))}
           <Button
            style={{
              marginTop: "20px",
              marginLeft: "280px",
              marginBottom: "20px",
              backgroundColor: "rgba(27,27,27,0.8)",
              color: "white",
            }}
            onClick={() => {
              history.goBack();
            }}
          >
            Go Back
          </Button>
        </TableBody>
      </Table>
    </TableContainer>
  );

}


export default WishListPage;
