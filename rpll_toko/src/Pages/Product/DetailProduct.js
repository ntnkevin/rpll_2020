import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import MainContex from "../mainContex";
import Button from "@material-ui/core/Button";
import * as ROUTES from "../../constants/routes";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import IconButton from "@material-ui/core/IconButton";
// import StarBorderIcon from '@material-ui/icons/StarBorder';

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "0px",
    marginBottom: "0px",
    paddingTop: "50px",
    width: "1000px",
    height: "560px",
    backgroundColor: "#fff",
  },
  content: {
    marginTop: "50px",
    paddingLeft: "70px",
  },
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  topTop: {
    marginTop: "50px",
  },
  gridList: {
    flexWrap: "nowrap",
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: "translateZ(0)",
  },
  title: {},
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
}));

const DetailProduct = () => {
  let { id } = useParams();
  const [menuList, setMenuList] = React.useState({ price: 0 });
  const [cartList, setCartList] = React.useState({ items: [] });
  const [otherMenuList, otherSetMenuList] = React.useState([]);
  const [wishList, setWishList] = React.useState({ items: [] });

  const contex = React.useContext(MainContex);
  const history = useHistory();
  const classes = useStyle();

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const document = await db.collection("product").doc(id).get();
    const item = {
      category: document.data().category,
      desc: document.data().description,
      name: document.data().name,
      price: document.data().price,
      photoUrl: document.data().url,
    };
    setMenuList(item);
  };

  const fetchCart = async () => {
    const db = firebase.firestore();
    var email = contex.email;
    await db
      .collection("cart")
      .where("owner", "==", email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            id: doc.id,
            items: doc.data().item,
            owner: doc.data().owner,
            price: doc.data().price,
            size: doc.size,
          };
          setCartList(item);
        });
      });
  };

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("product")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const barang = {
            id: doc.id,
            category: doc.data().category,
            description: doc.data().description,
            nama: doc.data().name,
            price: doc.data().price,
            weight: doc.data().weight,
            photoUrl: doc.data().url,
          };
          result.push(barang);
        });
      });
    otherSetMenuList(result);
  };

  const NonAdminUser = (props) => {
    return (
      <div>
        <Button
          size="small"
          color="white"
          onClick={() => {
            history.push(`/product/detail/${props.idProduct}`);
            window.location.reload(false);
          }}
        >
          View
        </Button>
      </div>
    );
  };

  const fetchWishList = async () => {
    const db = firebase.firestore();
    var email = contex.email;
    await db
      .collection("wishList")
      .where("owner", "==", email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            id: doc.id,
          };
          setWishList(item);
        });
      });
  };

  React.useEffect(() => {
    fetchProduct();
    fetchCart();
    fetchMenu();
    fetchWishList();
    //eslint-disable-next-line
  }, [contex.email]);

  const handlerBuy = () => {
    history.push(`/product/buy/${id}`);
  };

  const updateItems = async (cartId, item) => {
    const db = firebase.firestore();
    var tempe = await db.collection("cart").doc(cartId);
    tempe.update({ item: firebase.firestore.FieldValue.arrayUnion(item) });
  };

  const handlerAddToCart = () => {
    const newItem = {
      idProduct: id,
      nameProduct: menuList.name,
      qty: "1",
    };
    alert(`Cart Added !`);
    history.push(ROUTES.PRODUCT);
    updateItems(cartList.id, newItem);
    return console.log("ADD TO CHART CLICKED");
  };

  const updateWishList = async (wishId, item) => {
    const db = firebase.firestore();
    var tempe = await db.collection("wishList").doc(wishId);
    tempe.update({ items: firebase.firestore.FieldValue.arrayUnion(item) });
  };

  const handlerAddToWishList = () => {
    const newItem = {
      idProduct: id,
      nameProduct: menuList.name,
      url: menuList.photoUrl,
    };
    updateWishList(wishList.id, newItem);
    alert(`Wishlist Added !`);
    history.push(ROUTES.WISHLIST);
    return console.log("ADD TO CHART CLICKED");
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <div>
          <div className={classes.row}>
            <div className={classes.column}>
              <img
                src={menuList.photoUrl}
                width="400px"
                height="300px"
                alt="photoUrl"
                style={{ paddingLeft: "50px" }}
              />
            </div>
          </div>
          {console.log(cartList.size)}
          <h1>{menuList.name}</h1>
          <table style={{ marginRight: "50px" }}>
            <tr>
              <td>
                <p>Price</p>
              </td>
              <td>:</td>
              <td>
                <h1
                  style={{
                    color: "#fa591d",
                    display: "inline",
                  }}
                >
                  Rp.{numberWithCommas(menuList.price)},00
                </h1>
              </td>
            </tr>
            <tr>
              <td>
                <p>Category</p>
              </td>
              <td>:</td>
              <td>
                <h3>{menuList.category}</h3>
              </td>
            </tr>
            <tr>
              <td>
                <p>Description</p>
              </td>
              <td>:</td>
              <td>
                <p style={{ textAlign: "justify" }}>{menuList.desc}</p>
              </td>
            </tr>
          </table>
          <br></br>
          <Button
            variant="contained"
            className={classes.button}
            component="a"
            style={{
              marginLeft: "500px",
              width: "450px",
              color: "white",
              backgroundColor: "rgb(27,27,27,0.8)",
            }}
            onClick={handlerAddToWishList}
          >
            Add to WishList
          </Button>
          <br></br>
          <br></br>

          <Button
            variant="contained"
            className={classes.button}
            component="a"
            style={{
              marginLeft: "500px",
              width: "450px",
              color: "white",
              backgroundColor: "rgb(27,27,27,0.8)",
            }}
            onClick={handlerAddToCart}
          >
            Add to Cart
          </Button>
          <br></br>
          <br></br>
          <Button
            variant="contained"
            className={classes.button}
            component="a"
            style={{
              marginLeft: "500px",
              width: "450px",
              color: "white",
              backgroundColor: "rgb(27,27,27,0.8)",
            }}
            onClick={handlerBuy}
          >
            Buy
          </Button>
        </div>
      </div>
      <div className={classes.divbig}>
        <div>
          
          <div className={classes.topTop}>
          <h3>
            <center>Other Similar Product:</center>
          </h3>
            <Container className={classes.cardGrid} maxWidth="md">
              <GridList className={classes.gridList} cols={3.5}>
                {otherMenuList.map((barang, key) => (
                  (barang.category === menuList.category) ? (
                  <GridListTile key={barang.nama}>
                    <img src={barang.photoUrl} alt={barang.nama} />
                    <GridListTileBar
                      title={barang.nama}
                      classes={{
                        root: classes.titleBar,
                        title: classes.title,
                      }}
                      actionIcon={<NonAdminUser idProduct={barang.id} />}
                    ></GridListTileBar>
                  </GridListTile>
                  ) : ("None")
                ))}
              </GridList>
            </Container>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailProduct;
