import React from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import firebase from "firebase/app";
import "firebase/firestore";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import { Link, useHistory } from "react-router-dom";
import MainContex from "../mainContex";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Collapsible from 'react-collapsible';

import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(0),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  topTop: {
    marginTop: "0px",
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },

}));

const ProductPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const [menuList, setMenuList] = React.useState([]);
  const contex = React.useContext(MainContex);

  const [openTable, setOpenTable] = React.useState(false);
  const [openChair, setOpenChair] = React.useState(false);
  const [openLamp, setOpenLamp] = React.useState(false);
  const [openCarpet, setOpenCarpet] = React.useState(false);
  const [openBedFrame, setOpenBedFrame] = React.useState(false);
  const [openMattress, setOpenMattress] = React.useState(false);
  const [openKitchenAccessories, setOpenKitchenAccessories] = React.useState(false);
  
  const handleClickTable = () => {
    setOpenTable(!openTable);
  };
  const handleClickChair = () => {
    setOpenChair(!openChair);
  };
  const handleClickLamp = () => {
    setOpenLamp(!openLamp);
  };
  const handleClickCarpet = () => {
    setOpenCarpet(!openCarpet);
  };
  const handleClickBedFrame = () => {
    setOpenBedFrame(!openBedFrame);
  };
  const handleClickMattress = () => {
    setOpenMattress(!openMattress);
  };
  const handleClickKitchenAccessories = () => {
    setOpenKitchenAccessories(!openKitchenAccessories);
  };

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("product")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            id: doc.id,
            category: doc.data().category,
            description: doc.data().description,
            nama: doc.data().name,
            price: doc.data().price,
            weight: doc.data().weight,
            photoUrl: doc.data().url,
          };
          result.push(item);
        });
      });
    setMenuList(result);
  };

  React.useEffect(() => {
    fetchMenu();
  }, []);

  const refreshPage = () => {
    window.location.reload(false);
  };

  const deleteItem = async (idProduct) => {
    const db = firebase.firestore();
    await db.collection("product").doc(idProduct).delete();
    refreshPage();
  };

  const deleteHandler = (idProduct) => {
    confirmAlert({
      title: "Confirm to Delete Product",
      message: "Are you sure to do this?",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteItem(idProduct),
        },
        {
          label: "No",
          onClick: () => alert("Cancle to Delete"),
        },
      ],
    });
  };

  const AdminUser = (props) => {
    return (
      <div>
        <Button
          size="small"
          color="primary"
          onClick={() => {
            history.push(`/admin/edit-product/${props.idProduct}`);
          }}
        >
          Edit
        </Button>
        <Button
          size="small"
          color="primary"
          onClick={() => {
            deleteHandler(props.idProduct);
          }}
        >
          Delete
        </Button>
      </div>
    );
  };

  const NonAdminUser = (props) => {
    return (
      <div>
        <Button
          
          size="small"
          
          color="primary"
          onClick={() => {
            history.push(`/product/buy/${props.idProduct}`);
          }}
          
        >
          Buy
        </Button>
        
        <Button
          
          size="small"
          color="primary"
          onClick={() => {
            
            history.push(`/product/detail/${props.idProduct}`);
          }}
        >
          View Detail
        </Button>
      </div>
    );

    
  };



  return (
    <div>
      <CssBaseline />
      <main>
        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickTable}>
                <ListItemText primary="Table"/>
                 {openTable ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openTable} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Table") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h4">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
          
        </div>

        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickChair}>
            <ListItemText primary="Chair"/>
                 {openChair ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openChair} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Chair") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h4">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
          
        </div>

        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickLamp}>
            <ListItemText primary="Lamp"/>
                 {openLamp ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openLamp} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Lamp") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h4">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
          
        </div>

        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickCarpet}>
            <ListItemText primary="Carpet"/>
                 {openCarpet ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openCarpet} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Carpet") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h4">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
          
        </div>

        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickBedFrame}>
            <ListItemText primary="Bed Frame"/>
                 {openBedFrame ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openBedFrame} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Bed Frame") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h4">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
          
        </div>

        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickMattress}>
            <ListItemText primary="Mattress"/>
                 {openMattress ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openMattress} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Mattress") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h2">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
          
        </div>
        
        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
          <List>
            <ListItem button onClick={handleClickKitchenAccessories}>
            <ListItemText primary="Kitchen Accessories"/>
                 {openKitchenAccessories ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openKitchenAccessories} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                
                {/* <Collapsible trigger="Show More"> */}
                  <Grid container spacing={4}>
                    {menuList.map((item, key) => (
                      (item.category === "Kitchen Accessories") ? (
                      <Grid item key={key} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={item.photoUrl}
                            title={item.nama}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h5" component="h2">
                              {item.nama}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            {contex.email === "admin@gmail.com" ? (
                              <AdminUser idProduct={item.id} />
                            ) : (
                              <NonAdminUser idProduct={item.id} />
                            )}
                          </CardActions>
                        </Card>
                      </Grid>) : ("")
                    ))}
                  </Grid>
                </ListItem>
              </List>
            </Collapse>
            </List>
            <hr></hr>
          </Container>
        </div>
        


      </main>
    </div>
  );
};

export default ProductPage;
