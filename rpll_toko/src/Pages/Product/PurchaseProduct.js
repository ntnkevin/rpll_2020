import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import { useParams } from "react-router-dom";
import MainContex from "../mainContex";
import { makeStyles, Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import * as ROUTES from "../../constants/routes";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "25px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {
    marginTop: "15px",
    paddingLeft: "70px",
    paddingBottom: "30px",
  },
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
  tr: {},
  trr: {
    paddingLeft: "30px",
    paddingRight: "30px",
    borderRadius: "10px",
  },
  divimg: {
    paddingTop: "100px",
    width: "250px",
    paddingRight: "50px",
    height: "300px",
    backgroundColor: "white",
    textAlign: "center",
    verticalAlign: "middle",
  },
}));

const PurchaseProductPage = () => {
  let { id } = useParams();
  const [txtQty, setTxtQty] = React.useState("1");
  const [productList, setProductList] = React.useState({ price: 0 });
  const classes = useStyle();
  const history = useHistory();
  const contex = React.useContext(MainContex);

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const productRef = db.collection("product").doc(id);
    await productRef.get().then((doc) => {
      const item = {
        category: doc.data().category,
        description: doc.data().description,
        name: doc.data().name,
        price: doc.data().price,
        photoUrl: doc.data().url,
        weight: doc.data().weight,
      };
      setProductList(item);
    });
  };

  React.useEffect(() => {
    fetchProduct();
    //eslint-disable-next-line
  }, []);

  const product = {
    idProduct: id,
    nameProduct: productList.name,
    qty: txtQty,
  };

  const orderProduct = product;

  const totalPrice = () => {
    var total;
    var intPrice = parseInt(productList.price, 10);
    total = intPrice * txtQty;

    return total;
  };

  const addToChart = () => {
    var result = totalPrice();
    const db = firebase.firestore();
    db.collection("cart")
      .add({
        owner: contex.email,
        item: orderProduct,
        price: result,
      })
      .then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
        // history.push(`/payment/${docRef.id}`);
      });
  };

  const handlerSubmitLogin = () => {
    try {
      addToChart();
      alert("Order Created");
      history.push(ROUTES.LIST_CART);
    } catch (error) {
      console.log(error);
    }
  };

  const LoginUser = () => {
    return (
      <Button
        style={{
          marginLeft: "280px",
          backgroundColor: "rgba(27,27,27,0.8)",
          color: "white",
        }}
        onClick={handlerSubmitLogin}
      >
        Click To Buy!
      </Button>
    );
  };

  const handlerSubmitNonLogin = () => {
    alert("Please Login first to buy our product!");
    history.push(ROUTES.SIGN_IN);
  };

  const NonLogin = () => {
    return (
      <Button
        style={{
          marginLeft: "280px",
          backgroundColor: "rgba(27,27,27,0.8)",
          color: "white",
        }}
        onClick={handlerSubmitNonLogin}
      >
        Click To Buy!
      </Button>
    );
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <div className={classes.content}>
          <h1>Buy this Product</h1>
          <div>
            <table>
              <tr>
                <td className={classes.tr} width="250px">
                  <div className={classes.divimg}>
                    <img
                      src={productList.photoUrl}
                      width="150px"
                      height="100px"
                      alt="photoUrl"
                    ></img>
                  </div>
                </td>
                <td className={classes.trr} width="400px">
                  <h3>{productList.name}</h3>
                  <hr></hr>
                  <h3>
                    {/* {console.log(productList.price)}
                    {productList.price} */}
                    Rp. {numberWithCommas(productList.price)},00
                  </h3>
                  <hr></hr>
                  <label style={{ display: "inline" }}>QTY : </label>
                  <input
                    style={{ display: "inline" }}
                    type="text"
                    size="4"
                    value={txtQty}
                    onChange={(ev) => setTxtQty(ev.target.value)}
                  ></input>
                  <hr></hr>
                  <p style={{ textAlign: "justify" }}>
                    {productList.description}
                  </p>
                  <hr></hr>
                  <p style={{ textAlign: "justify" }}>
                    Weight {productList.weight} 
                  </p>
                </td>
              </tr>
            </table>
          </div>
          {contex.email !== "" ? <LoginUser /> : <NonLogin />}
          <Button
            style={{
              marginLeft: "50px",
              backgroundColor: "rgba(27,27,27,0.8)",
              color: "white",
            }}
            onClick={() => {
              history.goBack();
            }}
          >
            Cancel
          </Button>
        </div>
      </div>
    </div>
  );
};

export default PurchaseProductPage;
