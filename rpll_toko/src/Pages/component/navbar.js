import React from "react";
import * as ROUTES from "../../constants/routes";
import { Link, useHistory } from "react-router-dom";
import firebase from "firebase/app";
import "firebase/auth";
import MainContex from "../mainContex";
import { signOut } from "../../controller/authController";

const NavbarWithAuth = () => {
  const history = useHistory();
  var contex = React.useContext(MainContex);

  const signOutHandler = () => {
    signOut();
    contex.setEmail("");
    history.push(ROUTES.HOME);
  };

  const AdminUser = () => {
    return (
      <li className="has-children active" style={{ color: "black" }}>
        ADMIN MENU
        <ul className="dropdown">
          <li>
            <Link to={ROUTES.ADMIN_ADD_PRODUCT}>Add Product</Link>
          </li>
          <li>
            <Link to={ROUTES.ADMIN_ADD_VENDOR}>Add Vendor</Link>
          </li>
          <li>
            <Link to={ROUTES.ADMIN_VIEW_VENDOR}>View Vendor</Link>
          </li>
          <li>
            <Link to={ROUTES.ADMIN_CUSTOMER_SERVICE}>
              View Customer Service
            </Link>
          </li>
        </ul>
      </li>
    );
  };

  const NormalUser = () => {
    return (
      <li className="has-children active" style={{ color: "black" }}>
        CART
        <ul className="dropdown">
          <li>
            <Link to={ROUTES.LIST_CART}>List Cart</Link>
          </li>
          <li>
            <Link to={ROUTES.HISTORY_CART}>History Order</Link>
          </li>
        </ul>
      </li>
    );
  };

  return (
    <div>
      <div className="site-wrap">
        {/*  HEADER */}
        <div className="site-navbar bg-white py-2">
          <div className="search-wrap">
            <div className="container">
              <a href="#" className="search-close js-search-close">
                <span className="icon-close2"></span>
              </a>
              <form action="#" method="post">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search keyword and hit enter..."
                />
              </form>
            </div>
          </div>

          <div className="container">
            <div className="d-flex align-items-center justify-content-between">
              <div className="logoa">
                <div className="site-logo">
                  <div className="js-logo-clone">
                    <Link to={ROUTES.HOME}>ShopMax</Link>
                  </div>
                </div>
              </div>
              <div className="main-nav d-none d-lg-block">
                <nav
                  className="site-navigation text-right text-md-center"
                  role="navigation"
                >
                  <ul className="site-menu js-clone-nav d-none d-lg-block">
                    <li>
                      <Link to={ROUTES.HOME}>Home</Link>
                    </li>
                    <li>
                      <Link to={ROUTES.PRODUCT}>Product</Link>
                    </li>
                    {contex.email === "admin@gmail.com" ? (
                      <AdminUser />
                    ) : (
                      <NormalUser />
                    )}
                    <li
                      className="has-children active"
                      style={{ color: "black" }}
                    >
                      ABOUT
                      <ul className="dropdown">
                        <li>
                          <Link to={ROUTES.ABOUT}>About</Link>
                        </li>
                        <li>
                          <Link to={ROUTES.CONTACT}>Contact Us</Link>
                        </li>
                        <li>
                          <Link to={ROUTES.CUSTOMERSERVICE}>
                            Customer Service
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li
                      className="has-children active"
                      style={{ color: "black" }}
                    >
                      ACCOUNT
                      <ul className="dropdown">
                        <li>
                          <Link to={ROUTES.PROFILE}>Profile</Link>
                        </li>
                        <li>
                          <Link to={ROUTES.WISHLIST}>Wishlist</Link>
                        </li>
                        <li>
                          <Link to={ROUTES.HOME} onClick={signOutHandler}>
                            LogOut
                          </Link>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const NavbarNonAuth = () => {
  return (
    <div>
      <div className="site-wrap">
        {/*  HEADER */}
        <div className="site-navbar bg-white py-2">
          <div className="search-wrap">
            <div className="container">
              <a href="#" className="search-close js-search-close">
                <span className="icon-close2"></span>
              </a>
              <form action="#" method="post">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search keyword and hit enter..."
                />
              </form>
            </div>
          </div>
          <div className="container">
            <div className="d-flex align-items-center justify-content-between">
              <div className="logoa">
                <div className="site-logo">
                  <div className="js-logo-clone">
                    <Link to={ROUTES.HOME}>ShopMax</Link>
                  </div>
                </div>
              </div>
              <div className="main-nav d-none d-lg-block">
                <nav
                  className="site-navigation text-right text-md-center"
                  role="navigation"
                >
                  <ul className="site-menu js-clone-nav d-none d-lg-block">
                    <li>
                      <Link to={ROUTES.HOME}>Home</Link>
                    </li>
                    <li>
                      <Link to={ROUTES.PRODUCT}>Product</Link>
                    </li>
                    <li
                      className="has-children active"
                      style={{ color: "black" }}
                    >
                      ABOUT
                      <ul className="dropdown">
                        <li>
                          <Link to={ROUTES.ABOUT}>About</Link>
                        </li>
                        <li>
                          <Link to={ROUTES.CONTACT}>Contact Us</Link>
                        </li>
                        <li>
                          <Link to={ROUTES.CUSTOMERSERVICE}>
                            Customer Service
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <Link to={ROUTES.SIGN_IN}>SignIn</Link>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const Navbar = () => {
  const contex = React.useContext(MainContex);

  return (
    <div>
      {/* <p> CONTEX EMAIL USER = {contex.email}</p> */}
      {contex.email != "" ? <NavbarWithAuth /> : <NavbarNonAuth />}
    </div>
  );
};

export default Navbar;
