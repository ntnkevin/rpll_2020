import React from "react";

class DiscoverCollection extends React.Component {
  render() {
    return (
      <div className="site-section">
        <div className="container">
          <div className="title-section mb-5">
            <h2 className="text-uppercase">
              <span className="d-block">Discover</span> The Collections
            </h2>
          </div>
          <div className="row align-items-stretch">
            <div className="col-lg-8">
              <div className="product-item sm-height full-height bg-gray">
                <a href="/product" className="product-category">
                  Bed Room
                </a>
                <img
                  src="https://firebasestorage.googleapis.com/v0/b/rpll-2020.appspot.com/o/Wallpaper%2F122919_m_super_furniture_bed.jpg?alt=media&token=5123debb-09b6-49d1-b509-e968c5b9790d"
                  alt="Image"
                  className="img-fluid"
                />
              </div>
            </div>
            <div className="col-lg-4">
              <div className="product-item sm-height bg-gray mb-4">
                <a href="/product" className="product-category">
                  Kitchen Set
                </a>
                <img
                  src="https://firebasestorage.googleapis.com/v0/b/rpll-2020.appspot.com/o/Wallpaper%2F122919_m_super_furniture_dining.jpg?alt=media&token=3b08f75b-126b-4b3d-bd6d-006094b5d1dc"
                  alt="Image"
                  className="img-fluid"
                />
              </div>

              <div className="product-item sm-height bg-gray">
                <a href="/product" className="product-category">
                  Accessories
                </a>
                <img
                  src="https://firebasestorage.googleapis.com/v0/b/rpll-2020.appspot.com/o/Wallpaper%2F0810593_PE771335_S4.jpg?alt=media&token=15560520-9cb3-4d67-8ccb-f3780db8bf3e"
                  alt="Image"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DiscoverCollection;
