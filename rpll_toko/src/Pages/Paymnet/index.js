import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import * as ROUTES from "../../constants/routes";
import { useParams, useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";

const PaymentPage = () => {
  var { id } = useParams();
  const history = useHistory();
  const [itemsOrder, setItemOrder] = React.useState([]);
  const [qtyOrder, setQtyOrder] = React.useState([]);
  const [priceOrder, setPriceOrder] = React.useState([]);
  const [ownerOrder, setOwnerOrder] = React.useState(""); //nama customer
  const [priceOwner, setPriceOwner] = React.useState(""); //total billed


  const fetchMenu = async () => {
    const db = firebase.firestore();
    const document = await db.collection("order").doc(id).get();
    const productItemName= [];
    const qtyProduct = [];
    const priceProduct = [];

    const bill = {
      qty: document.data().item,
      product: document.data().item,
      price: document.data().item,
      owner: document.data().name,
      price: document.data().totalBilled,
    };
    
    document.data().item.map(product => {
      var name = (product.itemName);
      var qty = (product.itemQty);
      var price = (product.price);
      productItemName.push(name);
      qtyProduct.push(qty);
      priceProduct.push(price);
    })

    setQtyOrder(qtyProduct);
    setItemOrder(productItemName);
    setPriceOrder(priceProduct);
    setOwnerOrder(bill.owner);
    setPriceOwner(bill.price);
    
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, []);

  const payNow = () => {
    const db = firebase.firestore();
    db.collection("order").doc(id).update({ status: "PAID" });
    alert("Thank you for trusting us !");
    history.push(ROUTES.HISTORY_CART);
  };

  const payLater = () => {
    history.push(ROUTES.HOME);
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div>
      <center>
      <p><b>Billing Summary : </b> </p>
      <p> {ownerOrder}</p>
      <p><b>Grand Total</b></p>
      <p> Rp.{priceOwner} ,-</p>
      <p><b>Berikut Barang yang Dibeli: </b></p>
      <table border='1'>
        <tr>  
          <th>Nama Barang</th>
          <th>Jumlah</th>
          <th>Harga</th>
          
        </tr>      
          {itemsOrder.map((barang,i) => (
            <tr>
              <td> {barang}</td>
              <td>{qtyOrder[i]}</td>
              <td>{priceOrder[i]}</td>
            </tr>
          ))}
      </table>


    
      <p>{}</p>
      <Button
        size="large"
        onClick={payLater}
        style={{
          backgroundColor: "black",
          color: "white",
          marginRight: "50px",
        }}
      >
        Pay Later
      </Button>
      <Button
        size="large"
        onClick={payNow}
        style={{ backgroundColor: "black", color: "white" }}
      >
        Pay Now
      </Button>
      </center>
    </div>
  );
};

export default PaymentPage;
