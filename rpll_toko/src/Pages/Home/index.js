import React from "react";
import MainContex from "../mainContex";
import firebase from "firebase/app";
import "firebase/auth";

// IMPORT PAGES
// import Navbar from "../component/navbar";
import PopularProd from "../component/popularProduct";
import DiscoverCollection from "../component/discoverCollection";
import MostRated from "../component/mostRated";
import ProductCategory from "../component/ProductCategory";
import ProductHeader from "../component/ProductHeader";
import ProductHowItWork from "../component/ProductHowItWork";

const Home = () => {
  const contex = React.useContext(MainContex);
  return (
    <div>
      <div className="site-wrap">
        {console.log("EMAIL IN CINTEX : " + contex.email)};{/* <Navbar /> */}
        {/* MAIN 1 (ERROR GILANG) */}
        {/* <div className="site-blocks-cover" data-aos="fade">
          <div className="container">
            <div className="row">
              <div className="col-md-6 ml-auto order-md-2 align-self-start">
                <div className="site-block-cover-content">
                  <h2 className="sub-title">#New Summer Collection 2019</h2>
                  <h1>Arrivals Sales</h1>
                  <p>
                    <a href="#" className="btn btn-black rounded-0">
                      Shop Now
                    </a>
                  </p>
                </div>
              </div>
              <div className="col-md-6 order-1 align-self-end">
                <img
                  src={require("../../images/model_3.png")}
                  alt="Image"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
        </div> */}
        {/* END MAIN 1 */}
        <ProductHeader />
        <DiscoverCollection />
        <ProductCategory />
        <ProductHowItWork />
        {/* <PopularProd /> */}
        {/* <MostRated /> */}
        {/* MAIN 5 */}
        {/* <div className="site-blocks-cover inner-page py-5" data-aos="fade">
          <div className="container">
            <div className="row">
              <div className="col-md-6 ml-auto order-md-2 align-self-start">
                <div className="site-block-cover-content">
                  <h2 className="sub-title">#New Summer Collection 2019</h2>
                  <h1>New Shoes</h1>
                  <p>
                    <a href="#" className="btn btn-black rounded-0">
                      Shop Now
                    </a>
                  </p>
                </div>
              </div>
              <div className="col-md-6 order-1 align-self-end">
                <img
                  src={require("../../images/model_6.png")}
                  alt="Image"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
        </div> */}
        {/* END MAIN 5 */}
        {/* <Footer /> */}
      </div>
    </div>
  );
};

export default Home;
