import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import { makeStyles, Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "60px",
    paddingTop: "50px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {},
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));

const ListCustomerServicePage = () => {
  const classes = useStyle();
  const history = useHistory();
  const [menuList, setMenuList] = React.useState([]);

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("customerService")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            docId: doc.id,
            email: doc.data().email,
          };
          result.push(item);
        });
      });
    setMenuList(result);
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, []);

  const Order = () => {
    return (
      <div>
        <center>
          <table>
            <tr>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Doc Id</h1>
              </th>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Email </h1>
              </th>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>View Detail</h1>
              </th>
            </tr>
            {menuList.map((item, key) => (
              <tr key={key}>
                <td
                  style={{
                    borderRight: "1px solid black",
                    marginRight: "30px",
                  }}
                >
                  <h3>{item.docId}</h3>
                </td>
                <td
                  style={{
                    borderRight: "1px solid black",
                    paddingRight: "30px",
                    paddingLeft: "30px",
                  }}
                >
                  <h3>{item.email}</h3>
                </td>
                <td style={{ paddingRight: "30px", paddingLeft: "30px" }}>
                  <Button
                    style={{
                      backgroundColor: "rgba(27,27,27,0.8)",
                      color: "white",
                    }}
                    onClick={() => {
                      history.push(
                        `/admin/customer-service/detail/${item.docId}`
                      );
                    }}
                  >
                    View Detail
                  </Button>
                </td>
              </tr>
            ))}
          </table>
        </center>
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <h1>List Customer Service :</h1>
        <Order />
      </div>
    </div>
  );
};

export default ListCustomerServicePage;
