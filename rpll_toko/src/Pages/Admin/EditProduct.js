import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import * as ROUTES from "../../constants/routes";
import { useParams, useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const EditProductPage = () => {
  let { id } = useParams();
  const history = useHistory();
  const classes = useStyles();
  const [txtNameProduct, setTxtNameProduct] = React.useState("");
  const [txtDescProduct, setTxtDescProduct] = React.useState("");
  const [txtCategoryProduct, setTxtCategoryProduct] = React.useState("");
  const [txtPriceProduct, setTxtPriceProduct] = React.useState("");
  const [txtVendor, setTxtVendor] = React.useState("");
  const [txtWeightProduct, setTxtWeightProduct] = React.useState("");
  const [vendorList, setVendorList] = React.useState([]);

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const productRef = db.collection("product").doc(id);
    await productRef.get().then((doc) => {
      setTxtNameProduct(doc.data().name);
      setTxtDescProduct(doc.data().description);
      setTxtCategoryProduct(doc.data().category);
      setTxtPriceProduct(doc.data().price);
      setTxtWeightProduct(doc.data().weight);
      setTxtVendor(doc.data().vendor);
    });
  };

  const fetchFendor = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("vendor")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            name: doc.data().name,
          };
          result.push(item);
        });
      });
    setVendorList(result);
  };

  React.useEffect(() => {
    fetchProduct();
    fetchFendor();
    //eslint-disable-next-line
  }, []);

  const editData = () => {
    const db = firebase.firestore();
    db.collection("product").doc(id).update({
      category: txtCategoryProduct,
      description: txtDescProduct,
      name: txtNameProduct,
      price: txtPriceProduct,
      weight: txtWeightProduct,
      vendor: txtVendor,
    });
  };

  const handleCategory = (event) => {
    setTxtCategoryProduct(event.target.value);
  };

  const handleVendor = (event) => {
    setTxtVendor(event.target.value);
  };

  const editHandler = () => {
    editData();
    alert("Data Edited");
    history.push(ROUTES.PRODUCT);
  };

  return (
    <div style={{ marginTop: "80px", marginBottom: "100px" }}>
      <Container maxWidth="sm">
        <Typography variant="h6" gutterBottom>
          Edit Product id : {id}
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="productName"
              name="productName"
              label="Product Name"
              value={txtNameProduct}
              fullWidth
              onChange={(ev) => {
                setTxtNameProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="descName"
              name="descName"
              label="Description Name"
              value={txtDescProduct}
              fullWidth
              onChange={(ev) => {
                setTxtDescProduct(ev.target.value);
              }}
            />
          </Grid>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={txtCategoryProduct}
              onChange={handleCategory}
            >
              <MenuItem value="Table"> Table</MenuItem>
              <MenuItem value="Chair"> Chair</MenuItem>
              <MenuItem value="Lamp"> Lamp</MenuItem>
              <MenuItem value="Mattress"> Mattress</MenuItem>
              <MenuItem value="Cupboard"> Cupboard</MenuItem>
            </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Vendor</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={txtVendor}
              onChange={handleVendor}
            >
              {vendorList.map((item, key) => (
                <MenuItem key={key} value={item.name}>
                  {item.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <Grid item xs={12}>
            <TextField
              id="weightProduct"
              name="weightProduct"
              label="Weight Product"
              value={txtWeightProduct}
              fullWidth
              onChange={(ev) => {
                setTxtWeightProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="productProce"
              name="productPrice"
              label="Product Price"
              value={txtPriceProduct}
              fullWidth
              onChange={(ev) => {
                setTxtPriceProduct(ev.target.value);
              }}
            />
          </Grid>
          <Button
            style={{
              backgroundColor: "rgba(27,27,27,0.8)",
              color: "#fff",
              marginRight: "25px",
            }}
            onClick={() => {
              editHandler();
            }}
          >
            Edit
          </Button>
          <Button
            style={{ backgroundColor: "rgba(27,27,27,0.8)", color: "#fff" }}
            onClick={() => {
              history.goBack();
            }}
          >
            Cancel
          </Button>
        </Grid>
      </Container>
    </div>
  );
};

export default EditProductPage;
