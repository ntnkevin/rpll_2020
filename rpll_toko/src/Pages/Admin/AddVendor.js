import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
// if request.auth != null;

const AddVendorPage = () => {
  const [txtVendorName, setTxtVendorName] = React.useState("");
  const [txtVendorAddress, setTxtVendorAddress] = React.useState("");

  const updateDb = async () => {
    const db = firebase.firestore();
    await db.collection("vendor").add({
      address: txtVendorAddress,
      name: txtVendorName,
    });
  };

  const refreshPage = () => {
    window.location.reload(false);
  };

  const handleSubmit = async () => {
    try {
      await updateDb();
      alert(`VENDOR ADDED`);
      refreshPage();
      // console.log("Data Added");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div style={{ marginTop: "50px", marginBottom: "100px" }}>
      <Container maxWidth="sm">
        <Typography variant="h6" gutterBottom>
          Add Vendor
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="vendorName"
              name="vendorName"
              label="Vendor Name"
              value={txtVendorName}
              fullWidth
              onChange={(ev) => {
                setTxtVendorName(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="vendorAddress"
              name="vendorAddress"
              label="Vendor Address"
              value={txtVendorAddress}
              fullWidth
              onChange={(ev) => {
                setTxtVendorAddress(ev.target.value);
              }}
            />
          </Grid>
        </Grid>
        <br></br>
        <button onClick={handleSubmit}>Add Vendor</button>
      </Container>
    </div>
  );
};

export default AddVendorPage;
