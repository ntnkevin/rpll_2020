import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import { makeStyles, Button } from "@material-ui/core";
import { confirmAlert } from "react-confirm-alert";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "60px",
    paddingTop: "50px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {},
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));

const ListVendorPage = () => {
  const classes = useStyle();
  const [menuList, setMenuList] = React.useState([]);

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("vendor")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            vendorId: doc.id,
            name: doc.data().name,
            address: doc.data().address,
          };
          result.push(item);
        });
      });
    setMenuList(result);
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, []);

  const refreshPage = () => {
    window.location.reload(false);
  };

  const deleteItem = async (idVendor) => {
    const db = firebase.firestore();
    await db.collection("vendor").doc(idVendor).delete();
    refreshPage();
  };

  const deleteHandler = (idVendor) => {
    confirmAlert({
      title: "Confirm to Delete Vendor",
      message: "Are you sure to do this?",
      buttons: [
        {
          label: "Yes",
          onClick: () => deleteItem(idVendor),
        },
        {
          label: "No",
          onClick: () => alert("Cancle to Delete"),
        },
      ],
    });
  };

  const Order = () => {
    return (
      <div>
        <center>
          <table>
            <tr>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Vendor Name</h1>
              </th>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Vendor Address</h1>
              </th>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Delete Vendor</h1>
              </th>
            </tr>
            {menuList.map((item, key) => (
              <tr key={key}>
                <td
                  style={{
                    borderRight: "1px solid black",
                    marginRight: "30px",
                  }}
                >
                  <h3>{item.name}</h3>
                </td>
                <td
                  style={{
                    borderRight: "1px solid black",
                    paddingRight: "30px",
                    paddingLeft: "30px",
                  }}
                >
                  <h3>{item.address}</h3>
                </td>
                <td style={{ paddingRight: "30px", paddingLeft: "30px" }}>
                  <Button
                    style={{
                      backgroundColor: "rgba(27,27,27,0.8)",
                      color: "white",
                    }}
                    onClick={() => {
                      deleteHandler(item.vendorId);
                    }}
                  >
                    Delete Vendor
                  </Button>
                </td>
              </tr>
            ))}
          </table>
        </center>
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <h1>List Vendor :</h1>
        <Order />
      </div>
    </div>
  );
};

export default ListVendorPage;
