import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import { useParams, useHistory } from "react-router-dom";

const DetailCustService = () => {
  var { id } = useParams();
  const history = useHistory();
  const [custService, setCustService] = React.useState([]);

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("customerService")
      .doc(id)
      .get()
      .then((doc) => {
        const item = {
          email: doc.data().email,
          custTxt: doc.data().custTxt,
        };
        result.push(item);
      });
    setCustService(result);
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, []);

  return (
    <div>
      <h1> Hello </h1>
      {custService.map((item, key) => (
        <div key={key}>
          <p>Email : {item.email}</p>
          <p>Text Customer : {item.custTxt}</p>
        </div>
      ))}
      <button onClick={history.goBack}> Go Back</button>
    </div>
  );
};

export default DetailCustService;
