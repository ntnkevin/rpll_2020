import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
// if request.auth != null;

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const AddProductPage = () => {
  const [txtNameProduct, setTxtNameProduct] = React.useState("");
  const [txtDescProduct, setTxtDescProduct] = React.useState("");
  const [txtCategoryProduct, setTxtCategoryProduct] = React.useState("");
  const [txtPriceProduct, setTxtPriceProduct] = React.useState("");
  const [txtWeight, setTxtWeight] = React.useState("");
  const [txtVendor, setTxtVendor] = React.useState("");
  const [image, setImage] = React.useState(null);
  const [vendorList, setVendorList] = React.useState([]);
  const classes = useStyles();

  const fetchFendor = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("vendor")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            name: doc.data().name,
          };
          result.push(item);
        });
      });
    setVendorList(result);
  };

  React.useEffect(() => {
    fetchFendor();
  }, []);

  const updateDb = async (imgUrl) => {
    const db = firebase.firestore();
    await db.collection("product").add({
      category: txtCategoryProduct,
      description: txtDescProduct,
      name: txtNameProduct,
      price: txtPriceProduct,
      url: imgUrl,
      weight: txtWeight,
      vendor: txtVendor,
    });
  };

  const refreshPage = () => {
    window.location.reload(false);
  };

  const uploadImage = async () => {
    var storageRef = firebase.storage().ref(`Product/${image.name}`);
    await storageRef.put(image); //Proses Upload!
    const imgUrl = await storageRef.getDownloadURL();

    await updateDb(imgUrl);
  };

  const handleSubmit = async () => {
    try {
      await uploadImage();
      alert(`PRODUCT ADDED`);
      refreshPage();
      // console.log("Data Added");
    } catch (error) {
      console.log(error);
    }
  };

  const handleCategory = (event) => {
    setTxtCategoryProduct(event.target.value);
  };

  const handleVendor = (event) => {
    setTxtVendor(event.target.value);
  };

  const handleFile = (event) => {
    if (event.target.files[0]) {
      const img = event.target.files[0];
      setImage(img);
    }
  };

  return (
    <div style={{ marginTop: "50px", marginBottom: "100px" }}>
      <Container maxWidth="sm">
        <Typography variant="h6" gutterBottom>
          Add Product
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="productName"
              name="productName"
              label="Product Name"
              value={txtNameProduct}
              fullWidth
              onChange={(ev) => {
                setTxtNameProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="descName"
              name="descName"
              label="Description Name"
              value={txtDescProduct}
              fullWidth
              onChange={(ev) => {
                setTxtDescProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="weightProduct"
              name="weightProduct"
              label="Weight Product"
              value={txtWeight}
              fullWidth
              onChange={(ev) => {
                setTxtWeight(ev.target.value);
              }}
            />
          </Grid>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={txtCategoryProduct}
              onChange={handleCategory}
            >
              <MenuItem value="Table"> Table</MenuItem>
              <MenuItem value="Chair"> Chair</MenuItem>
              <MenuItem value="Lamp"> Lamp</MenuItem>
              <MenuItem value="Mattress"> Mattress</MenuItem>
              <MenuItem value="Bed Frame"> Bed Frame</MenuItem>
              <MenuItem value="Carpet"> Carpet</MenuItem>
              <MenuItem value="Kitchen Accessories"> Kitchen Accessories</MenuItem>
              
            </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Vendor</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={txtVendor}
              onChange={handleVendor}
            >
              {vendorList.map((item, key) => (
                <MenuItem key={key} value={item.name}>
                  {item.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <Grid item xs={12}>
            <TextField
              id="productProce"
              name="productPrice"
              label="Product Price"
              value={txtPriceProduct}
              fullWidth
              onChange={(ev) => {
                setTxtPriceProduct(ev.target.value);
              }}
            />
          </Grid>
        </Grid>
        <br></br>
        <input
          type="file"
          onChange={handleFile}
          accept="image/*"
          style={{ paddingRight: "200px" }}
        />
        <button onClick={handleSubmit}>Add Product</button>
      </Container>
    </div>
  );
};

export default AddProductPage;
