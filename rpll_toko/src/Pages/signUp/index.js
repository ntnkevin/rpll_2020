import React from "react";
import firebase from "firebase/app";
import "firebase/auth";

import { useHistory } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import {
  addDataToDb,
  signUp,
  editUserData,
  createUser,
  addWishLish,
} from "../../controller/authController";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = () => {
  const classes = useStyles();
  const [txtEmail, setTxtEmail] = React.useState("");
  const [txtName, setTxtName] = React.useState("");
  const [txtPassword, setTxtPassword] = React.useState("");
  const [txtAddress, setTxtAddress] = React.useState("");
  const [txtPhoneNumber, setTxtPhoneNumber] = React.useState("");

  const createUser = async () => {
    const dataUser = {
      name: txtName,
      password: txtPassword,
      email: txtEmail,
      alamat: txtAddress,
      noTelepon: txtPhoneNumber,
    };

    try {
      addDataToDb(dataUser);
      await firebase
        .auth()
        .createUserWithEmailAndPassword(txtEmail, txtPassword);
      console.log("Register to auth done");
      alert("Register Done");
      // IF CREATE USER SUCCESS, THEN EXECUTE FUNCTION BELOW
      signUp(dataUser);
      editUserData(dataUser);
      addWishLish(dataUser);
    } catch (error) {
      console.log(error);
      throw error;
    }
  };

  const handleSubmit = async () => {
    createUser()
      .then(async (result) => {
        console.log(result);
        history.push(ROUTES.HOME);
      })
      .catch((error) => {
        alert(`Cannot register : ${error}`);
      });
  };

  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}></Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                type="text"
                autoComplete="fname"
                name="username"
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                autoFocus
                value={txtName}
                onChange={(ev) => setTxtName(ev.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                type="text"
                value={txtEmail}
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={(ev) => setTxtEmail(ev.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="phonenumber"
                label="Phone Number"
                id="phonenumber"
                autoComplete="current-phonenumber"
                type="text"
                value={txtPhoneNumber}
                onChange={(ev) => setTxtPhoneNumber(ev.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="address"
                label="Address"
                name="address"
                autoComplete="lname"
                type="text"
                value={txtAddress}
                onChange={(ev) => setTxtAddress(ev.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                type="password"
                value={txtPassword}
                onChange={(ev) => setTxtPassword(ev.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            onClick={handleSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Button
            onClick={goBack}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Cancel
          </Button>
          <Grid container justify="flex-end">
            <Grid item></Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default SignUp;
