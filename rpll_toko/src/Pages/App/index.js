import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import { MainProvider } from "../mainContex";
import firebaseConfig from "../Firebase";
import firebase from "firebase/app";

// Import Pages
import HomePages from "../Home";
import SignIn from "../SignIn";
import Product from "../Product";
import SignUp from "../signUp";
import Profile from "../profile";
import Admin from "../Admin";
import Navbar from "../component/navbar";
import Footer from "../component/footer";
import BuyProduct from "../Product/PurchaseProduct";
import DetailProduct from "../Product/DetailProduct";
import HistoryCart from "../Cart/HistoryCart";
import ListCart from "../Cart/ListCart";
import AboutPage from "../About";
import ContactPage from "../Contact";
import CustomerServicePage from "../CustomerService";
import AdminAddProduct from "../Admin/AddProduct";
import AdminAddVendor from "../Admin/AddVendor";
import AdminViewVendor from "../Admin/ListVendor";
import AdminDetailCustService from "../Admin/DetailCustomerService";
import EditProduct from "../Admin/EditProduct";
import AdminViewCustomerService from "../Admin/ListCustomerService";
import PaymentPage from "../Paymnet";
import WishListPage from "../WishList";

//import CSS
import "../../css/bootstrap/bootstrap-grid.css";
import "../../css/bootstrap/bootstrap-reboot.css";
import "../../css/aos.css";
import "../../css/bootstrap.min.css";
import "../../css/style.css";
import "../../css/magnific-popup.css";

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

function App() {
  return (
    <div className="App">
      <Router>
        <MainProvider>
          <Navbar />
          <Switch>
            <Route exact path={ROUTES.HOME} component={HomePages} />
            <Route exact path={ROUTES.SIGN_IN} component={SignIn} />
            <Route exact path={ROUTES.SIGN_UP} component={SignUp} />
            <Route exact path={ROUTES.PRODUCT} component={Product} />
            <Route exact path={ROUTES.PROFILE} component={Profile} />
            <Route exact path={ROUTES.ABOUT} component={AboutPage} />
            <Route exact path={ROUTES.CONTACT} component={ContactPage} />
            <Route exact path={ROUTES.WISHLIST} component={WishListPage} />
            <Route
              exact
              path={ROUTES.CUSTOMERSERVICE}
              component={CustomerServicePage}
            />
            <Route exact path={ROUTES.HISTORY_CART} component={HistoryCart} />
            <Route exact path={ROUTES.LIST_CART} component={ListCart} />
            <Route exact path={ROUTES.ADMIN} component={Admin} />
            <Route
              exact
              path={ROUTES.ADMIN_ADD_PRODUCT}
              component={AdminAddProduct}
            />
            <Route
              exact
              path={ROUTES.ADMIN_ADD_VENDOR}
              component={AdminAddVendor}
            />
            <Route
              exact
              path={ROUTES.ADMIN_VIEW_VENDOR}
              component={AdminViewVendor}
            />
            <Route
              exact
              path={ROUTES.ADMIN_CUSTOMER_SERVICE}
              component={AdminViewCustomerService}
            />
            <Route
              path="/admin/customer-service/detail/:id"
              component={AdminDetailCustService}
            />
            <Route path="/admin/edit-product/:id" component={EditProduct} />
            <Route path="/product/buy/:id" component={BuyProduct} />
            <Route path="/product/detail/:id" component={DetailProduct} />
            <Route path="/payment-order/:id" component={PaymentPage} />
          </Switch>
          <Footer />
        </MainProvider>
      </Router>
    </div>
  );
}

export default App;
